//
//  EventBusConstants.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 9. 1..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class EventBusConstants {
    static let CONTENT_UPLOAD: String = "contentUpload"
    static let CONTENT_DELETE: String = "contentDelete"
}
