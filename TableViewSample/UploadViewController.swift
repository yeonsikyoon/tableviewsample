//
//  UploadViewController.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 24..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftEventBus

class UploadViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Vars
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var contentImageView: UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    var isModify: Bool = false
    var contentData: ContentData?

    override func viewDidLoad() {
        super.viewDidLoad()

        initNavigationView()
        
        imagePicker.delegate = self
        
        if (contentData != nil) {
            titleTextField.text = contentData?.title
            contentTextView.text = contentData?.content
        }
    }

    func initNavigationView() {
        let uploadViewTitle: String = contentData == nil ? "업로드" : "수정하기"
        navigationView.chagneTitle(title: uploadViewTitle)
        navigationView.setUseLeftButton(isUse: true)
        navigationView.setUseRightButton(isUse: true)
        
        self.navigationView.leftButton.addTarget(self, action: #selector(closeView(sender:)), for: .touchUpInside)
        self.navigationView.rightButton.addTarget(self, action: #selector(upload(sender:)), for: .touchUpInside)
    }
    
    func closeView(sender: Any?) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func upload(sender: Any?) {
        if (isModify) {
            let parameters: Parameters = [
                "id": contentData?.id! ?? Int(),
                "title": titleTextField.text!,
                "content": contentTextView.text!
            ]
            Alamofire.request(kUpdateContent, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString {
                response in
                self.responseResult(response: response)
            }
        } else {
            let parameters: Parameters = [
                "title": titleTextField.text!,
                "content": contentTextView.text!
            ]
            Alamofire.request(kInsertContent, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString {
                response in
                self.responseResult(response: response)
            }
        }
    }
    
    func setEntity(entity: ContentData?) {
        if entity != nil {
            isModify = true
            contentData = entity
        }
    }
    
    func responseResult(response: DataResponse<String>) {
        debugPrint(response)
        if ("success" == response.result.value) {
            
            contentData?.title = titleTextField.text
            contentData?.content = contentTextView.text
            
            let detailCommentViewController: DetailCommentViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailCommentViewController") as! DetailCommentViewController
            detailCommentViewController.setEntity(entity: contentData)
            
            SwiftEventBus.post(EventBusConstants.CONTENT_UPLOAD)
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - File Upload
    
    // https://solarianprogrammer.com/2017/05/02/swift-alamofire-tutorial-uploading-downloading-images/
    
    // http://www.codingexplorer.com/choosing-images-with-uiimagepickercontroller-in-swift/
    
    @IBAction
    func clickImageAddButton(sender: Any?) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            contentImageView.contentMode = .scaleAspectFit
            contentImageView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func upload(image: UIImage,
                progressCompletion: @escaping (_ percent: Float) -> Void,
                completion: @escaping (_ tags: [String]/*, _ colors: [PhotoColor]*/) -> Void) {
//        guard let imageData = UIImageJPEGRepresentation(image, 0.5) else {
//            print("Could not get JPEG representation of UIImage")
//            return
//        }
//        
//        let parameters = ["user":"Sol", "password":"secret1234"]
        
        let imageToUploadURL = Bundle.main.url(forResource: "tree", withExtension: "png")

        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(imageToUploadURL!, withName: "image")
//                for (key, val) in parameters {
//                    multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
//                    }
        },
            to: kInsertContent,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress { progress in
                        progressCompletion(Float(progress.fractionCompleted))
                    }
                    upload.validate()
                    upload.responseJSON { response in
                        debugPrint(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
                /*
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.appendBodyPart(data: imageData, name: "imagefile", fileName: "image.jpg", mimeType: "image/jpeg")
            },
            to: "http://api.imagga.com/v1/content",
            encodingCompletion: { encodingResult in
                // encodingCompletion클로져 부분
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress { progress in
                        progressCompletion(Float(progress.fractionCompleted))
                    }
                    upload.validate()
                    upload.responseJSON { response in
                        debugPrint(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        )*/
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
