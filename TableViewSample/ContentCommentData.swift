//
//  ContentCommentData.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 25..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class ContentCommentData {
    var contentId: Int?
    var id: Int?
    var profileImage: UIImage?
    var profileImagePath: String?
    var userName: String?
    var comment: String?
    var createdDateInt: Int?
    var createdDate: Date?
    var createdDateString: String? {
        get {
            if createdDate == nil {
                if createdDateInt == nil {
                    return nil
                } else {
                    let timeInterval = Double(createdDateInt!)
                    createdDate = Date(timeIntervalSince1970: timeInterval)
                }
                return nil
            }
            
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd HH:mm"
            
            return dateFormatter.string(from: createdDate!)
        }
    }
}
