//
//  DataHelper.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 7. 24..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class DataHelper {
    
    // MARK: - Vars
    
    static let sharedInstance = DataHelper()
    
    var contentList: NSMutableArray! = []
    var contentCommentList: NSMutableArray! = []
    
    // MARK: - Life Cycle
    
    func setup() {
        print("Data Helper Start")
        
//        self.initDummyList()
    }
    
    // MARK: - Dummy
    
    func initDummyList() {
        self.contentList.removeAllObjects()
        addDummyItems()
        
        self.contentCommentList.removeAllObjects()
        addDummyComments()
    }
    
    func addDummyItems() {
        let contentData1 = ContentData()
        contentData1.id = 0
        contentData1.userName = "박영배"
        contentData1.profileImage = UIImage.init(named: "image_0")
        contentData1.content = "2교시 학교에 갔다.\n2교시 사회시간\n오늘은 휴대폰이 필요한 시간이라 폰을 주셨다.\n하지만\n사회쌤 께들 켰 다 .\n선생님께서는 나 유섭 윤이 에게 죄를 물으셨다><ㅠㅜㅠㅜㅠ\n그래서!!\n나는 휴대폰으로 일본에 대해찾으라는 쌤의\n말씀을 듣지않고\n게임을했다><"
        contentData1.contentImage = nil
        contentData1.likeCount = 123456.withComma
        contentData1.commentCount = 120.withComma
        contentData1.createdDate = Date.init()
        
        let contentData2 = ContentData()
        contentData2.id = 1
        contentData2.userName = "이유림"
        contentData2.profileImage = UIImage.init(named: "image_0")
        contentData2.content = "이것은 두번째 내용입니다요"
        contentData2.contentImage = UIImage.init(named: "image_0")
        contentData2.likeCount = 123456.withComma
        contentData2.commentCount = 120.withComma
        contentData2.createdDate = Date.init()
        
        let contentData3 = ContentData()
        contentData3.id = 2
        contentData3.userName = "문지윤"
        contentData3.profileImage = UIImage.init(named: "image_0")
        contentData3.content = nil
        contentData3.contentImage = UIImage.init(named: "image_1")
        contentData3.likeCount = 123456.withComma
        contentData3.commentCount = 120.withComma
        contentData3.createdDate = Date.init()
        
        let contentData4 = ContentData()
        contentData4.id = 3
        contentData4.userName = "윤연식"
        contentData4.profileImage = UIImage.init(named: "image_0")
        contentData4.content = "dsakljflkdsajflksadjklfjlksadjflkasdjlkfjaklsjflkdasjklfjslkajfdkljsakldfjklsajlkfdjklsadjflk"
        contentData4.contentImage = UIImage.init(named: "image_2")
        contentData4.likeCount = 123456.withComma
        contentData4.commentCount = 120.withComma
        contentData4.createdDate = Date.init()
        
        
        self.contentList.add(contentData1)
        self.contentList.add(contentData2)
        self.contentList.add(contentData3)
        self.contentList.add(contentData4)
    }
    
    func addDummyComments() {
        let commentData1 = ContentCommentData()
//        commentData1.contentId = 1
        commentData1.id = 1
        commentData1.userName = "윤연식"
        commentData1.profileImage = UIImage.init(named: "image_0")
        commentData1.comment = "이것은 첫번째 내용입니다요"
        commentData1.createdDate = Date.init()
        
        let commentData2 = ContentCommentData()
        //        commentData2.contentId = 1
        commentData2.id = 2
        commentData2.userName = "윤연식"
        commentData2.profileImage = UIImage.init(named: "image_0")
        commentData2.comment = "이것은 두번째 내용입니다요"
        commentData2.createdDate = Date.init()
        
        let commentData3 = ContentCommentData()
        //        commentData3.contentId = 1
        commentData3.id = 1
        commentData3.userName = "윤연식"
        commentData3.profileImage = UIImage.init(named: "image_0")
        commentData3.comment = "이것은 세번째 내용입니다요"
        commentData3.createdDate = Date.init()
        
        
        self.contentCommentList.add(commentData1)
        self.contentCommentList.add(commentData2)
        self.contentCommentList.add(commentData3)
    }
    
    func getContentCount() -> Int {
        return self.contentList.count
    }
    
    func getContentCommentCount() -> Int {
        return self.contentCommentList.count
    }
}
