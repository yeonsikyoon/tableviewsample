//
//  ToastUtil.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 27..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class ToastUtil {
    
    static let sharedInstance = ToastUtil()
    
    func showToastShort(message : String, targetView: UIView) {
        showToast(message, targetView, 2.0)
    }
    
    func showToastLong(message : String, targetView: UIView) {
        showToast(message, targetView, 4.0)
    }
    
    func showToast(_ message : String, _ targetView: UIView, _ withDuration: TimeInterval) {
        let toastLabel = UILabel(frame: CGRect(x: targetView.frame.size.width/2 - 75, y: targetView.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        targetView.addSubview(toastLabel)
        UIView.animate(withDuration: withDuration, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
