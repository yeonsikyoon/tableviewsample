//
//  DetailViewController.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 7. 29..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    // MARK: - Vars
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var createDateLabel: UILabel!
    
    var contentData: ContentData = ContentData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initNavigationView()
        initImageView()
        initLabels()
        setProfileImageCircle()
        
        setDataInViews()
    }
    
    func initNavigationView() {
        self.navigationView.chagneTitle(title: "상세화면")
        self.navigationView.setUseLeftButton(isUse: true)
        self.navigationView.leftButton.addTarget(self, action: #selector(closeView(sender:)), for: .touchUpInside)
        
        // TODO 내 아이디와 같을 경우 수정하기 버튼을 보여준다.
        if (String(describing: contentData.id) == UserDefaults.standard.string(forKey: kUserId)) {
            self.navigationView.setUseRightButton(isUse: true)
            self.navigationView.rightButton.addTarget(self, action: #selector(closeView(sender:)), for: .touchUpInside)
        }
    }
    
    func initImageView() {
        self.contentImageView.contentMode = .scaleAspectFill
    }
    
    func initLabels() {
        self.contentLabel.font = UIFont.init(name: kDefaultFont, size: 14.0)
        self.contentLabel.numberOfLines = 0 // ?
        self.contentLabel.textColor = kTitleColor
    }
    
    func setProfileImageCircle() {
        self.profileImageView.layer.borderWidth = 1
        self.profileImageView.layer.masksToBounds = false
        self.profileImageView.layer.borderColor = UIColor.black.cgColor
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
        self.profileImageView.clipsToBounds = true
    }
    
    func closeView(sender: Any?) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func moveToUploadView(sender: Any?) {
        let uploadViewController: UploadViewController = self.storyboard?.instantiateViewController(withIdentifier: "UploadViewController") as! UploadViewController
        
        uploadViewController.setEntity(entity: contentData)
        
        self.navigationController?.pushViewController(uploadViewController, animated: true)
    }
    
    func setEntity(entity: ContentData) {
        contentData = entity
    }
    
    func setDataInViews() {
        self.profileImageView.image = contentData.profileImage
        self.userNameLabel.text = contentData.userName
        self.contentLabel.text = contentData.content
        self.contentImageView.image = contentData.contentImage
        self.createDateLabel.text = contentData.createdDateString
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
