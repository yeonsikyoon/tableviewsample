//
//  ContentCell.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 7. 24..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class ContentCell: UITableViewCell {
    
    // MARK: - Vars
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var deleteContentButton: UIButton!
    
    weak var delegate: ContentCellDelegate!
    var contentData: ContentData!
    
    
    // MARK: - Life Cycle
    
    func initVars() {
        self.clipsToBounds = true
    }
    
    func initBackgroundView() {
        self.backgroundView = UIView.init()
        self.backgroundView?.backgroundColor = kWhiteHighlightColor
        self.selectedBackgroundView = UIView.init()
        self.selectedBackgroundView?.backgroundColor = kWhiteHighlightColor
    }
    
    func initLabels() {
        self.contentLabel.font = UIFont.init(name: kDefaultFont, size: 14.0)
        self.contentLabel.numberOfLines = 0 // ?
        self.contentLabel.textColor = kTitleColor
    }
    
    func initImageView() {
        self.contentImageView.contentMode = .scaleAspectFill
    }
    
    func setProfileImageCircle() {
        self.profileImageView.layer.borderWidth = 1
        self.profileImageView.layer.masksToBounds = false
        self.profileImageView.layer.borderColor = UIColor.black.cgColor
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
        self.profileImageView.clipsToBounds = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initVars()
        self.initBackgroundView()
        self.initLabels()
        self.initImageView()
        
        self.setProfileImageCircle()
    }
    
    // MARK: - Entity
    
    func setEntity(entity: ContentData) {
        self.contentData = entity
        self.profileImageView.image = entity.profileImage
        self.userNameLabel.text = entity.userName
        self.contentLabel.text = entity.content
        self.contentImageView.image = entity.contentImage
        self.createdDateLabel.text = entity.createdDateString
    }
    
    @IBAction func clickContentContentButton(_ sender: Any) {
        if delegate == nil {
            return
        }
        delegate.deleteContent(contentData: contentData)
    }
    
    func getHeight(entity: ContentData) -> CGFloat {
        var cellHeight: CGFloat = 0.0
        
        if entity.contentImage != nil {
            cellHeight = CGFloat(kImageHeight)
        }
        
        if let content = entity.content {
            let maxLabelWidth: CGFloat = UIScreen.main.bounds.size.width - 64.0
            let contentLabelFrame = NSString.init(string: content).boundingRect(with: CGSize.init(width: maxLabelWidth, height: 0), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : self.contentLabel.font], context: nil)
            
            
            cellHeight += contentLabelFrame.height
        }
        
        if let likeCount = entity.likeCount {
            let maxLabelWidth: CGFloat = UIScreen.main.bounds.size.width - 64.0
            let likeCountLabelFrame = NSString.init(string: likeCount).boundingRect(with: CGSize.init(width: maxLabelWidth, height: 0), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : self.contentLabel.font], context: nil)
            
            cellHeight += likeCountLabelFrame.height
        }
        
        if let createdDate = entity.createdDateString {
            let maxLabelWidth: CGFloat = UIScreen.main.bounds.size.width - 64.0
            let createdDateLabelFrame = NSString.init(string: createdDate).boundingRect(with: CGSize.init(width: maxLabelWidth, height: 0), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : self.contentLabel.font], context: nil)
            
            
            cellHeight += createdDateLabelFrame.height
        }
        
        cellHeight += 30/*좋아요 버튼*/ + 40/*프로필 이미지*/ + 70.0 /*Margin,..*/
        
        return cellHeight
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

protocol ContentCellDelegate: class {
    func deleteContent(contentData: ContentData)
}
