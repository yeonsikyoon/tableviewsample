//
//  AlertGenerator.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 31..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class AlertUtil {
    
    /*
        사용 예시 :
        AlertUtil.showAlert(viewController: self, title: "title", message: "message", okButtonText: "ok", cancleButtonText: "cancel", completion: {
        (result) -> () in
        print(result)
        })
     */
    static func showAlert(viewController: UIViewController, title: String, message: String,
             okButtonText: String, cancleButtonText: String,
             completion: @escaping (_ result: Bool) -> ()) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okButtonText, style: UIAlertActionStyle.default) {
            UIAlertAction in
            completion(true)
        }
        let cancelAction = UIAlertAction(title: cancleButtonText, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            completion(false)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}
