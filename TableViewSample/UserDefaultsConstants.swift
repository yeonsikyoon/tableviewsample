//
//  UserDefaultsConstants.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 26..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

let kUserId = "userId"
let kUserName = "userName"
let kUserProfileImagePath = "userProfileImagePath"
let kIsLogin = "isLogin"
