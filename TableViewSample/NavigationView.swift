//
//  NavigationView.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 22..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class NavigationView: UIView {
    var view: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initailize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initailize()
    }
    
    func initailize() {
        self.view = Bundle.main.loadNibNamed("NavigationView", owner: self, options: nil)?.first as! UIView!
        self.view.frame = self.bounds
        self.addSubview(view)
        
        leftButton.isHidden = true
        rightButton.isHidden = true
    }
    
    func chagneTitle(title: String) {
        titleLabel.text = title
    }
    
    func setUseLeftButton(isUse: Bool) {
        leftButton.isHidden = !isUse
    }
    
    func setUseRightButton(isUse: Bool) {
        rightButton.isHidden = !isUse
    }
    
}

