//
//  ViewController.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 7. 23..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit
import Alamofire
import SwiftEventBus

let kDetailContentCellID = "detailContentCellID"
let kDetailCommentCellID = "detailCommentCellID"

class DetailCommentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
        ContentCellDelegate {
    
    // MARK: - Vars
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var loadIndicatorView: UIView!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var commentInputTextView: UITextView!
    @IBOutlet weak var commentUploadButton: UIButton!
    
    var loadMoreTimer: Timer?
    var isLoadingItems: Bool = false
    var isEnd: Bool = false
        
    var contentData: ContentData? = ContentData()
    var contentCommentList: NSMutableArray! = []
    
    func setEntity(entity: ContentData?) {
        contentData = entity
    }
    
    // MARK: - Life Cycle
    
    func initVars() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func initBackgroundView() {
        self.view.backgroundColor = kWhiteHighlightColor
    }
    
    func initNibs() {
        let contentCellNib: UINib = UINib.init(nibName: "ContentCell", bundle: nil)
        self.mainTableView.register(contentCellNib, forCellReuseIdentifier: kDetailContentCellID)
        
        let commentCellNib: UINib = UINib.init(nibName: "ContentCommentCell", bundle: nil)
        self.mainTableView.register(commentCellNib, forCellReuseIdentifier: kDetailCommentCellID)
    }
    
    func initNavi() {
        self.navigationView.chagneTitle(title: "상세화면")
        self.navigationView.setUseLeftButton(isUse: true)
        self.navigationView.leftButton.addTarget(self, action: #selector(closeView(sender:)), for: .touchUpInside)
        // TODO 내 아이디와 같을 경우 수정하기 버튼을 보여준다.
//        if (String(describing: contentData.id) == UserDefaults.standard.string(forKey: kUserId)) {
            self.navigationView.setUseRightButton(isUse: true)
            self.navigationView.rightButton.addTarget(self, action: #selector(moveToUploadView(sender:)), for: .touchUpInside)
//        }
    }
    
    func initTableView() {
        self.mainTableView.separatorStyle = .none
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView.backgroundColor = kClearColor
        self.mainTableView.contentInset = UIEdgeInsets.init(top: 8.0, left: 0.0, bottom: 8.0, right: 0.0)
        
        setShowLoadIndicator(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 키보드 올라갈 때 화면이 가려지지 않게 화면도 같이 올라가도록 함
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // 화면 클릭시에 키보드 내려가도록 하기
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailCommentViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        self.initVars()
        self.initBackgroundView()
        self.initNibs()
        self.initNavi()
        self.initTableView()
        
        requestContentCommentList()
    }
    
    func requestContentCommentList() {
        if (isEnd) {
            return
        }
        isLoadingItems = true
        let parameters: Parameters = ["b_id":contentData?.id! ?? Int()]
        Alamofire.request(kContentCommentList, parameters: parameters).responseJSON {
            response in
            
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if ("SUCCESS" != String(describing: response.result)) {
                self.isLoadingItems = false
                self.isEnd = true
                return
            }
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                let array = json as! NSArray
                for item in array {
                    let obj = item as! NSDictionary
                    let id: String = obj["id"] as! String
                    let name: String = obj["name"] as! String
                    let content: String = obj["content"] as! String
                    let date: String = obj["date"] as! String
                    
                    let contentCommentData = ContentCommentData()
                    contentCommentData.id = Int(id);
                    contentCommentData.comment = content;
                    contentCommentData.createdDateInt = Int(date);
                    
                    contentCommentData.userName = name
                    contentCommentData.profileImage = UIImage.init(named: "image_0")
                    
                    self.contentCommentList.add(contentCommentData)
                }// for
            }// if
            
            self.mainTableView.reloadData()
            self.isLoadingItems = false
        }// Alamofire.response
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.mainTableView.reloadData()
    }
    
    func closeView(sender: Any?) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func moveToUploadView(sender: Any?) {
        let uploadViewController: UploadViewController = self.storyboard?.instantiateViewController(withIdentifier: "UploadViewController") as! UploadViewController
        uploadViewController.setEntity(entity: contentData)
        
        self.navigationController?.pushViewController(uploadViewController, animated: true)
    }
    
    func deleteContent(contentData: ContentData) {
        showContentDeletePopup()
    }
    
    func showContentDeletePopup() {
        AlertUtil.showAlert(viewController: self, title: "글 삭제", message: "해당 글을 삭제하시겠습니까?", okButtonText: "네", cancleButtonText: "아니오", completion: {
            (result) -> () in
            if result {
                let parameters: Parameters = [
                    "id": self.contentData!.id!
                ]
                Alamofire.request(kDeleteContent, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString {
                    response in
                    
                    debugPrint(response)
                    if ("success" == response.result.value) {
                        SwiftEventBus.post(EventBusConstants.CONTENT_DELETE, sender: self.contentData!)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        })
    }
    
    func showContentCommentDeletePopup(indexPath: IndexPath, contentCommentData: ContentCommentData) {
        AlertUtil.showAlert(viewController: self, title: "댓글 삭제", message: "해당 댓글을 삭제하시겠습니까?", okButtonText: "네", cancleButtonText: "아니오", completion: {
            (result) -> () in
            if result {
                let parameters: Parameters = [
                    "id": contentCommentData.id!
                ]
                Alamofire.request(kDeleteContentComment, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString {
                    response in
                    
                    debugPrint(response)
                    if ("success" == response.result.value) {
                        self.contentCommentList.remove(contentCommentData)
                        self.mainTableView.deleteRows(at: [indexPath], with: .automatic)
                    }
                }
            }
        })
    }
    
    func dismissKeyboard() {
        self.commentInputTextView.endEditing(true)
//        self.commentInputTextView.resignFirstResponder()
    }
    
    @IBAction func clickUploadComment(_ sender: Any) {
        // TODO 댓글 서버에 전송하기
        
        if (!isValidCommentData()) {
            ToastUtil.sharedInstance.showToastShort(message: "댓글을 입력해주세요.", targetView: self.view)
            showCommentAlertPopup()
            return
        }
        
        requestAddComment()
    }
    
    func isValidCommentData() -> Bool {
        return !self.commentInputTextView.text.isEmpty
    }
    
    func requestAddComment() {
        // TODO 서버로 댓글 입력
        
        let parameters: Parameters = [
            "b_id": contentData?.id! ?? Int(),
            "name": "윤연식",
            "content": commentInputTextView.text!
        ]
        Alamofire.request(kInsertContentComment, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString {
            response in
            self.responseResult(response: response)
        }
    }

    func responseResult(response: DataResponse<String>) {
        debugPrint(response)
        if ("success" == response.result.value) {
            
            self.commentInputTextView.text = ""
            
            //        self.commentInputTextView.endEditing(true)
            self.commentInputTextView.resignFirstResponder()
            
            self.contentCommentList.removeAllObjects()
            requestContentCommentList()
        }
    }
    
    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // TODO 본문 개수 1 + 댓글 수
        return self.contentCommentList.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            // 본문
            let contentCell: ContentCell = self.tableView(tableView, cellForRowAt: indexPath) as! ContentCell
            
            return contentCell.getHeight(entity: contentData!)
        } else {
            // 댓글
            let commentCell: ContentCommentCell = self.tableView(tableView, cellForRowAt: indexPath) as! ContentCommentCell
            
            let commentData: ContentCommentData = self.contentCommentList[indexPath.row-1] as! ContentCommentData
            
            return commentCell.getHeight(entity: commentData)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let contentCell: ContentCell = tableView.dequeueReusableCell(withIdentifier: kDetailContentCellID) as! ContentCell
        
            contentCell.delegate = self
            
            contentCell.setEntity(entity: contentData!)
            
//            contentCell.deleteContentButton.addTarget(self, action: #selector(showContentDeletePopup(sender:)), for: .touchUpInside)
            
            return contentCell
        } else {
            let commentCell: ContentCommentCell = tableView.dequeueReusableCell(withIdentifier: kDetailCommentCellID) as! ContentCommentCell
            let commentData: ContentCommentData = self.contentCommentList[indexPath.row-1] as! ContentCommentData
            
            commentCell.setEntity(entity: commentData)
            
            return commentCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = contentCommentList.count/* - 1 // 리스트에 본문도 한개 포함이 되므로 -1를 빼지 않는다. */
        if indexPath.row == lastElement && !isLoadingItems {
//            isLoadingItems = true
//            setShowLoadIndicator(true)
//            runLoadMoreTimer()
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let contentCommentData: ContentCommentData = (self.contentCommentList[indexPath.row-1] as! ContentCommentData)
        showContentCommentDeletePopup(indexPath: indexPath, contentCommentData: contentCommentData)
    }
    
    func runLoadMoreTimer() {
        loadMoreTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(loadMoreItems), userInfo: nil, repeats: false)
    }
    
    func loadMoreItems() {
        DataHelper.sharedInstance.addDummyComments()
        self.mainTableView.reloadData()
        isLoadingItems = false
        setShowLoadIndicator(false)
    }
    
    func setShowLoadIndicator(_ isSHow: Bool) {
        if isSHow {
            self.loadIndicatorView.isHidden = false
            self.loadIndicator.startAnimating()
        } else {
            self.loadIndicatorView.isHidden = true
            self.loadIndicator.stopAnimating()
        }
    }
    
    func showCommentAlertPopup() {
        let dialog = UIAlertController(title: "댓글", message: "댓글을 입력해 주세요.", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "확인", style: UIAlertActionStyle.default)
        dialog.addAction(action)
        
        self.present(dialog, animated: true, completion: nil)
    }
    
    // MARk : - Keyboard
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}

