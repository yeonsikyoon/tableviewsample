//
//  ViewController.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 7. 23..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit
import Alamofire
import SwiftEventBus

let kContentCellID = "contentCellID"
let kSegDetailViewID = "segDetailViewID"

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ContentCellDelegate {
    
    // MARK: - Vars
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var loadIndicatorView: UIView!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    
    var loadMoreTimer: Timer?
    var isLoadingItems: Bool = false
    
    
    // MARK: - Life Cycle
    
    func initVars() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func initBackgroundView() {
        self.view.backgroundColor = kWhiteHighlightColor
    }
    
    func initNibs() {
        let contentCellNib: UINib = UINib.init(nibName: "ContentCell", bundle: nil)
        self.mainTableView.register(contentCellNib, forCellReuseIdentifier: kContentCellID)
    }
    
    func initNavi() {
        self.navigationView.chagneTitle(title: "윤스타그램")
        self.navigationView.setUseRightButton(isUse: true)
        self.navigationView.rightButton.addTarget(self, action: #selector(moveToUploadView(sender:)), for: .touchUpInside)
    }
    
    func initTableView() {
        self.mainTableView.separatorStyle = .none
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView.backgroundColor = kClearColor
        self.mainTableView.contentInset = UIEdgeInsets.init(top: 8.0, left: 0.0, bottom: 8.0, right: 0.0)
        
        setShowLoadIndicator(false)
    }
    
    func setEventBus() {
        SwiftEventBus.onMainThread(self, name: EventBusConstants.CONTENT_UPLOAD) { result in
            DataHelper.sharedInstance.contentList.removeAllObjects()
            self.requestContentList()
        }
        
        SwiftEventBus.onMainThread(self, name: EventBusConstants.CONTENT_DELETE) { result in
            let content: ContentData = result.object as! ContentData
            
            for data in DataHelper.sharedInstance.contentList {
                if (data as! ContentData).id == content.id {
                    DataHelper.sharedInstance.contentList.remove(data)
                    break
                }
            }
            self.mainTableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initVars()
        self.initBackgroundView()
        self.initNibs()
        self.initNavi()
        self.initTableView()
        self.setEventBus()
        
        requestContentList()
    }
    
    func requestContentList() {
        let parameters: Parameters = [:]
        Alamofire.request(kContentList, parameters: parameters, encoding: URLEncoding.httpBody).responseJSON {
            response in
            
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if ("SUCCESS" != String(describing: response.result)) {
                return
            }
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                let array = json as! NSArray
                for item in array {
                    let obj = item as! NSDictionary
                    let id: String = obj["id"] as! String
                    let title: String = obj["title"] as! String
                    let content: String = obj["content"] as! String
                    let date: String = obj["date"] as! String
                    
                    let contentData = ContentData()
                    contentData.id = Int(id);
                    contentData.title = title;
                    contentData.content = content;
                    contentData.createdDateInt = Int(date);
                    
                    contentData.userName = "윤연식"
                    contentData.profileImage = UIImage.init(named: "image_0")
                    contentData.contentImage = UIImage.init(named: "image_2")
                    contentData.likeCount = 123456.withComma
                    contentData.commentCount = 120.withComma

                    
                    DataHelper.sharedInstance.contentList.add(contentData)
                }// for
            }// if
            
            self.mainTableView.reloadData()
        }// Alamofire.response
    }
    
    func moveToUploadView(sender: Any?) {
        let uploadViewController: UploadViewController = self.storyboard?.instantiateViewController(withIdentifier: "UploadViewController") as! UploadViewController
        
        self.navigationController?.pushViewController(uploadViewController, animated: true)
    }
    
    func deleteContent(contentData: ContentData) {
        showContentDeletePopup(contentData: contentData)
    }
    
    func showContentDeletePopup(contentData: ContentData) {
        AlertUtil.showAlert(viewController: self, title: "글 삭제", message: "해당 글을 삭제하시겠습니까?", okButtonText: "네", cancleButtonText: "아니오", completion: {
            (result) -> () in
            if result {
                let parameters: Parameters = [
                    "id": contentData.id!
                ]
                Alamofire.request(kDeleteContent, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString {
                    response in
                    
                    debugPrint(response)
                    if ("success" == response.result.value) {
                        SwiftEventBus.post(EventBusConstants.CONTENT_DELETE, sender: contentData)
                    }
                }
            }
        })
    }

    
    // MARK: - TableView

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataHelper.sharedInstance.contentList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell: ContentCell = self.tableView(tableView, cellForRowAt: indexPath) as! ContentCell
        let contentData: ContentData = DataHelper.sharedInstance.contentList[indexPath.row] as! ContentData
        
        return cell.getHeight(entity: contentData)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ContentCell = tableView.dequeueReusableCell(withIdentifier: kContentCellID) as! ContentCell
        let contentData: ContentData = DataHelper.sharedInstance.contentList[indexPath.row] as! ContentData
        
        cell.delegate = self
        
        cell.setEntity(entity: contentData)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = DataHelper.sharedInstance.contentList.count - 1
        if indexPath.row == lastElement && !isLoadingItems {
//            isLoadingItems = true
//            setShowLoadIndicator(true)
//            runLoadMoreTimer()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailCommentViewController: DetailCommentViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailCommentViewController") as! DetailCommentViewController
        
        let contentData: ContentData = DataHelper.sharedInstance.contentList[indexPath.row] as! ContentData
        
        detailCommentViewController.setEntity(entity: contentData)
        
        self.navigationController?.pushViewController(detailCommentViewController, animated: true)
    }
    
    func runLoadMoreTimer() {
        loadMoreTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(loadMoreItems), userInfo: nil, repeats: false)
    }
    
    func loadMoreItems() {
        DataHelper.sharedInstance.addDummyItems()
        self.mainTableView.reloadData()
        isLoadingItems = false
        setShowLoadIndicator(false)
    }
    
    func setShowLoadIndicator(_ isSHow: Bool) {
        if isSHow {
            self.loadIndicatorView.isHidden = false
            self.loadIndicator.startAnimating()
        } else {
            self.loadIndicatorView.isHidden = true
            self.loadIndicator.stopAnimating()
        }
    }
    
}

