//
//  Paths.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 30..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

let kUrl = "http://192.168.1.12"

let kContentList = kUrl + "/board.php?type=get"
let kInsertContent = kUrl + "/board.php?type=post"
let kUpdateContent = kUrl + "/board.php?type=update"
let kDeleteContent = kUrl + "/board.php?type=delete"
let kContentCommentList = kUrl + "/comment.php?type=get"
let kInsertContentComment = kUrl + "/comment.php?type=post"
let kUpdateContentComment = kUrl + "/comment.php?type=update"
let kDeleteContentComment = kUrl + "/comment.php?type=delete"
