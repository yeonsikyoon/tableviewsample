//
//  Extensions.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 7. 25..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import Foundation

extension Int {
    var withComma: String {
        let decimalFormatter = NumberFormatter()
        decimalFormatter.numberStyle = NumberFormatter.Style.decimal
        decimalFormatter.groupingSeparator = ","
        decimalFormatter.groupingSize = 3
        
        return decimalFormatter.string(from: self as NSNumber)!
    }
}

extension String {
    var delComma: String {
        if self.characters.count > 0 {
            return self.replacingOccurrences(of: ",", with: "")
        } else {
            return "0"
        }
    }
}

// MARK: - UserDefaults

// https://swifter.kr/2016/12/31/swift-3-0%EC%97%90%EC%84%9C-userdefaults-%EC%82%AC%EC%9A%A9%ED%95%98%EA%B8%B0/
/*
protocol KeyNamespaceable {
    func namespaced<T : RawRepresentable>(_ key: T) -> String
}

extension KeyNamespaceable {
    func namespaced<T: RawRepresentable>(_ key: T) -> String {
        return "\(Self.self).\(key.rawValue)"
    }
}

protocol StringDefaultSettable : KeyNamespaceable {
    associatedtype StringKey : RawRepresentable
}

extension StringDefaultSettable where StringKey.RawValue == String {
    func set(_ value: String, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    @discardableResult
    func string(forKey key: StringKey) -> String? {
        let key = namespaced(key)
        return UserDefaults.standard.string(forKey: key)
    }
}

extension UserDefaults : StringDefaultSettable {
    enum StringKey : String {
        case id
    }
}
 */
