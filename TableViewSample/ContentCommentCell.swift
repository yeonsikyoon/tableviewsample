//
//  ContentCommentCell.swift
//  TableViewSample
//
//  Created by 윤연식 on 2017. 8. 27..
//  Copyright © 2017년 YoonYeonSik. All rights reserved.
//

import UIKit

class ContentCommentCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    
    func initVars() {
        self.clipsToBounds = true
    }
    
    func initBackgroundView() {
        self.backgroundView = UIView.init()
        self.backgroundView?.backgroundColor = kWhiteHighlightColor
        self.selectedBackgroundView = UIView.init()
        self.selectedBackgroundView?.backgroundColor = kWhiteHighlightColor
    }
    
    func initLabels() {
        self.commentLabel.font = UIFont.init(name: kDefaultFont, size: 14.0)
        self.commentLabel.numberOfLines = 0 // ?
        self.commentLabel.textColor = kTitleColor
    }
    
    func setProfileImageCircle() {
        self.profileImageView.layer.borderWidth = 1
        self.profileImageView.layer.masksToBounds = false
        self.profileImageView.layer.borderColor = UIColor.black.cgColor
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
        self.profileImageView.clipsToBounds = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initVars()
        self.initBackgroundView()
        self.initLabels()
        
        self.setProfileImageCircle()
    }
    
    // MARK: - Entity
    
    func setEntity(entity: ContentCommentData) {
        self.profileImageView.image = entity.profileImage
        self.userNameLabel.text = entity.userName
        self.commentLabel.text = entity.comment
        self.createdDateLabel.text = entity.createdDateString
    }
    
    func getHeight(entity: ContentCommentData) -> CGFloat {
        var cellHeight: CGFloat = 0.0
        
        if let comment = entity.comment {
            let maxLabelWidth: CGFloat = UIScreen.main.bounds.size.width - 64.0
            let commentLabelFrame = NSString.init(string: comment).boundingRect(with: CGSize.init(width: maxLabelWidth, height: 0), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : self.commentLabel.font], context: nil)
            
            
            cellHeight += commentLabelFrame.height
        }
        
        if let createdDate = entity.createdDateString {
            let maxLabelWidth: CGFloat = UIScreen.main.bounds.size.width - 64.0
            let createdDateLabelFrame = NSString.init(string: createdDate).boundingRect(with: CGSize.init(width: maxLabelWidth, height: 0), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : self.commentLabel.font], context: nil)
            
            
            cellHeight += createdDateLabelFrame.height
        }
        
        cellHeight += 40/*프로필 이미지*/ + 60.0
        
        return cellHeight
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
